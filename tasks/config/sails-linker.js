/**
 * `sails-linker`
 *
 * ---------------------------------------------------------------
 *
 * Automatically inject <script> tags and <link> tags into the specified
 * specified HTML and/or EJS files.  The specified delimiters (`startTag`
 * and `endTag`) determine the insertion points.
 *
 * #### Development (default)
 * By default, tags will be injected for your app's client-side JavaScript files,
 * CSS stylesheets, and precompiled client-side HTML templates in the `templates/`
 * directory (see the `jst` task for more info on that).  In addition, if a LESS
 * stylesheet exists at `assets/styles/importer.less`, it will be compiled to CSS
 * and a `<link>` tag will be inserted for it.  Similarly, if any Coffeescript
 * files exists in `assets/js/`, they will be compiled into JavaScript and injected
 * as well.
 *
 * #### Production (`NODE_ENV=production`)
 * In production, all stylesheets are minified into a single `.css` file (see
 * `tasks/config/cssmin.js` task) and all client-side scripts are minified into
 * a single `.js` file (see `tasks/config/uglify.js` task).  Any client-side HTML
 * templates, CoffeeScript, or LESS files are bundled into these same two minified
 * files as well.
 *
 * For usage docs see:
 *   https://github.com/Zolmeister/grunt-sails-linker
 *
 */
module.exports = function(grunt) {

  grunt.config.set('sails-linker', {
    devJs: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsFilesToInject,
        'views/**/*.html': require('../pipeline').jsFilesToInject,
        'views/**/*.ejs': require('../pipeline').jsFilesToInject
      }
    },

    devJsRelative: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsFilesToInject,
        'views/**/*.html': require('../pipeline').jsFilesToInject,
        'views/**/*.ejs': require('../pipeline').jsFilesToInject
      }
    },

    prodJs: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['.tmp/public/min/production.min.js'],
        'views/**/*.html': ['.tmp/public/min/production.min.js'],
        'views/**/*.ejs': ['.tmp/public/min/production.min.js']
      }
    },

    prodJsRelative: {
      options: {
        startTag: '<!--SCRIPTS-->',
        endTag: '<!--SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': ['.tmp/public/min/production.min.js'],
        'views/**/*.html': ['.tmp/public/min/production.min.js'],
        'views/**/*.ejs': ['.tmp/public/min/production.min.js']
      }
    },

    devStyles: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public'
      },

      files: {
        '.tmp/public/**/*.html': require('../pipeline').cssFilesToInject,
        'views/**/*.html': require('../pipeline').cssFilesToInject,
        'views/**/*.ejs': require('../pipeline').cssFilesToInject
      }
    },

    devStylesRelative: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public',
        relative: true
      },

      files: {
        '.tmp/public/**/*.html': require('../pipeline').cssFilesToInject,
        'views/**/*.html': require('../pipeline').cssFilesToInject,
        'views/**/*.ejs': require('../pipeline').cssFilesToInject
      }
    },

    prodStyles: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/min/production.min.css'],
        'views/**/*.html': ['.tmp/public/min/production.min.css'],
        'views/**/*.ejs': ['.tmp/public/min/production.min.css']
      }
    },

    prodStylesRelative: {
      options: {
        startTag: '<!--STYLES-->',
        endTag: '<!--STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/min/production.min.css'],
        'views/**/*.html': ['.tmp/public/min/production.min.css'],
        'views/**/*.ejs': ['.tmp/public/min/production.min.css']
      }
    },

    // Bring in JST template object
    devTpl: {
      options: {
        startTag: '<!--TEMPLATES-->',
        endTag: '<!--TEMPLATES END-->',
        fileTmpl: '<script type="text/javascript" src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/jst.js'],
        'views/**/*.html': ['.tmp/public/jst.js'],
        'views/**/*.ejs': ['.tmp/public/jst.js']
      }
    },

    devJsJade: {
      options: {
        startTag: '// SCRIPTS',
        endTag: '// SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': require('../pipeline').jsFilesToInject
      }
    },

    devJsRelativeJade: {
      options: {
        startTag: '// SCRIPTS',
        endTag: '// SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': require('../pipeline').jsFilesToInject
      }
    },

    prodJsJade: {
      options: {
        startTag: '// SCRIPTS',
        endTag: '// SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production.min.js']
      }
    },

    prodJsRelativeJade: {
      options: {
        startTag: '// SCRIPTS',
        endTag: '// SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production.min.js']
      }
    },

    devStylesJade: {
      options: {
        startTag: '// STYLES',
        endTag: '// STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public'
      },

      files: {
        'views/**/*.jade': require('../pipeline').cssFilesToInject
      }
    },

    devStylesRelativeJade: {
      options: {
        startTag: '// STYLES',
        endTag: '// STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public',
        relative: true
      },

      files: {
        'views/**/*.jade': require('../pipeline').cssFilesToInject
      }
    },

    prodStylesJade: {
      options: {
        startTag: '// STYLES',
        endTag: '// STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production.min.css']
      }
    },

    prodStylesRelativeJade: {
      options: {
        startTag: '// STYLES',
        endTag: '// STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production.min.css']
      }
    },

    // Bring in JST template object
    devTplJade: {
      options: {
        startTag: '// TEMPLATES',
        endTag: '// TEMPLATES END',
        fileTmpl: 'script(type="text/javascript", src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/jst.js']
      }
    },







    devAdminJs: {
      options: {
        startTag: '<!--ADMIN_SCRIPTS-->',
        endTag: '<!--ADMIN_SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsAdminFilesToInject,
        'views/**/*.html': require('../pipeline').jsAdminFilesToInject,
        'views/**/*.ejs': require('../pipeline').jsAdminFilesToInject
      }
    },

    devAdminJsRelative: {
      options: {
        startTag: '<!--ADMIN_SCRIPTS-->',
        endTag: '<!--ADMIN_SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsAdminFilesToInject,
        'views/**/*.html': require('../pipeline').jsAdminFilesToInject,
        'views/**/*.ejs': require('../pipeline').jsAdminFilesToInject
      }
    },

    prodAdminJs: {
      options: {
        startTag: '<!--ADMIN_SCRIPTS-->',
        endTag: '<!--ADMIN_SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['.tmp/public/min/production-admin.min.js'],
        'views/**/*.html': ['.tmp/public/min/production-admin.min.js'],
        'views/**/*.ejs': ['.tmp/public/min/production-admin.min.js']
      }
    },

    prodAdminJsRelative: {
      options: {
        startTag: '<!--ADMIN_SCRIPTS-->',
        endTag: '<!--ADMIN_SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': ['.tmp/public/min/production-admin.min.js'],
        'views/**/*.html': ['.tmp/public/min/production-admin.min.js'],
        'views/**/*.ejs': ['.tmp/public/min/production-admin.min.js']
      }
    },

    devAdminStyles: {
      options: {
        startTag: '<!--ADMIN_STYLES-->',
        endTag: '<!--ADMIN_STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public'
      },

      files: {
        '.tmp/public/**/*.html': require('../pipeline').cssAdminFilesToInject,
        'views/**/*.html': require('../pipeline').cssAdminFilesToInject,
        'views/**/*.ejs': require('../pipeline').cssAdminFilesToInject
      }
    },

    devAdminStylesRelative: {
      options: {
        startTag: '<!--ADMIN_STYLES-->',
        endTag: '<!--ADMIN_STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public',
        relative: true
      },

      files: {
        '.tmp/public/**/*.html': require('../pipeline').cssAdminFilesToInject,
        'views/**/*.html': require('../pipeline').cssAdminFilesToInject,
        'views/**/*.ejs': require('../pipeline').cssAdminFilesToInject
      }
    },

    prodAdminStyles: {
      options: {
        startTag: '<!--ADMIN_STYLES-->',
        endTag: '<!--ADMIN_STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/min/production-admin.min.css'],
        'views/**/*.html': ['.tmp/public/min/production-admin.min.css'],
        'views/**/*.ejs': ['.tmp/public/min/production-admin.min.css']
      }
    },

    prodAdminStylesRelative: {
      options: {
        startTag: '<!--ADMIN_STYLES-->',
        endTag: '<!--ADMIN_STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/min/production-admin.min.css'],
        'views/**/*.html': ['.tmp/public/min/production-admin.min.css'],
        'views/**/*.ejs': ['.tmp/public/min/production-admin.min.css']
      }
    },

    // Bring in JST template object
    devAdminTpl: {
      options: {
        startTag: '<!--ADMIN_TEMPLATES-->',
        endTag: '<!--ADMIN_TEMPLATES END-->',
        fileTmpl: '<script type="text/javascript" src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/jst-admin.js'],
        'views/**/*.html': ['.tmp/public/jst-admin.js'],
        'views/**/*.ejs': ['.tmp/public/jst-admin.js']
      }
    },

    devAdminJsJade: {
      options: {
        startTag: '// ADMIN_SCRIPTS',
        endTag: '// ADMIN_SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': require('../pipeline').jsAdminFilesToInject
      }
    },

    devAdminJsRelativeJade: {
      options: {
        startTag: '// ADMIN_SCRIPTS',
        endTag: '// ADMIN_SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': require('../pipeline').jsAdminFilesToInject
      }
    },

    prodAdminJsJade: {
      options: {
        startTag: '// ADMIN_SCRIPTS',
        endTag: '// ADMIN_SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production-admin.min.js']
      }
    },

    prodAdminJsRelativeJade: {
      options: {
        startTag: '// ADMIN_SCRIPTS',
        endTag: '// ADMIN_SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production-admin.min.js']
      }
    },

    devAdminStylesJade: {
      options: {
        startTag: '// ADMIN_STYLES',
        endTag: '// ADMIN_STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public'
      },

      files: {
        'views/**/*.jade': require('../pipeline').cssAdminFilesToInject
      }
    },

    devAdminStylesRelativeJade: {
      options: {
        startTag: '// ADMIN_STYLES',
        endTag: '// ADMIN_STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public',
        relative: true
      },

      files: {
        'views/**/*.jade': require('../pipeline').cssAdminFilesToInject
      }
    },

    prodAdminStylesJade: {
      options: {
        startTag: '// ADMIN_STYLES',
        endTag: '// ADMIN_STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production-admin.min.css']
      }
    },

    prodAdminStylesRelativeJade: {
      options: {
        startTag: '// ADMIN_STYLES',
        endTag: '// ADMIN_STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production-admin.min.css']
      }
    },

    // Bring in JST template object
    devAdminTplJade: {
      options: {
        startTag: '// ADMIN_TEMPLATES',
        endTag: '// ADMIN_TEMPLATES END',
        fileTmpl: 'script(type="text/javascript", src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/jst-admin.js']
      }
    },





    devUserJs: {
      options: {
        startTag: '<!--USER_SCRIPTS-->',
        endTag: '<!--USER_SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsUserFilesToInject,
        'views/**/*.html': require('../pipeline').jsUserFilesToInject,
        'views/**/*.ejs': require('../pipeline').jsUserFilesToInject
      }
    },

    devUserJsRelative: {
      options: {
        startTag: '<!--USER_SCRIPTS-->',
        endTag: '<!--USER_SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': require('../pipeline').jsUserFilesToInject,
        'views/**/*.html': require('../pipeline').jsUserFilesToInject,
        'views/**/*.ejs': require('../pipeline').jsUserFilesToInject
      }
    },

    prodUserJs: {
      options: {
        startTag: '<!--USER_SCRIPTS-->',
        endTag: '<!--USER_SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/**/*.html': ['.tmp/public/min/production-user.min.js'],
        'views/**/*.html': ['.tmp/public/min/production-user.min.js'],
        'views/**/*.ejs': ['.tmp/public/min/production-user.min.js']
      }
    },

    prodUserJsRelative: {
      options: {
        startTag: '<!--USER_SCRIPTS-->',
        endTag: '<!--USER_SCRIPTS END-->',
        fileTmpl: '<script src="%s"></script>',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/**/*.html': ['.tmp/public/min/production-user.min.js'],
        'views/**/*.html': ['.tmp/public/min/production-user.min.js'],
        'views/**/*.ejs': ['.tmp/public/min/production-user.min.js']
      }
    },

    devUserStyles: {
      options: {
        startTag: '<!--USER_STYLES-->',
        endTag: '<!--USER_STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public'
      },

      files: {
        '.tmp/public/**/*.html': require('../pipeline').cssUserFilesToInject,
        'views/**/*.html': require('../pipeline').cssUserFilesToInject,
        'views/**/*.ejs': require('../pipeline').cssUserFilesToInject
      }
    },

    devUserStylesRelative: {
      options: {
        startTag: '<!--USER_STYLES-->',
        endTag: '<!--USER_STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public',
        relative: true
      },

      files: {
        '.tmp/public/**/*.html': require('../pipeline').cssUserFilesToInject,
        'views/**/*.html': require('../pipeline').cssUserFilesToInject,
        'views/**/*.ejs': require('../pipeline').cssUserFilesToInject
      }
    },

    prodUserStyles: {
      options: {
        startTag: '<!--USER_STYLES-->',
        endTag: '<!--USER_STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/min/production-user.min.css'],
        'views/**/*.html': ['.tmp/public/min/production-user.min.css'],
        'views/**/*.ejs': ['.tmp/public/min/production-user.min.css']
      }
    },

    prodUserStylesRelative: {
      options: {
        startTag: '<!--USER_STYLES-->',
        endTag: '<!--USER_STYLES END-->',
        fileTmpl: '<link rel="stylesheet" href="%s">',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/min/production-user.min.css'],
        'views/**/*.html': ['.tmp/public/min/production-user.min.css'],
        'views/**/*.ejs': ['.tmp/public/min/production-user.min.css']
      }
    },

    // Bring in JST template object
    devUserTpl: {
      options: {
        startTag: '<!--USER_TEMPLATES-->',
        endTag: '<!--USER_TEMPLATES END-->',
        fileTmpl: '<script type="text/javascript" src="%s"></script>',
        appRoot: '.tmp/public'
      },
      files: {
        '.tmp/public/index.html': ['.tmp/public/jst-user.js'],
        'views/**/*.html': ['.tmp/public/jst-user.js'],
        'views/**/*.ejs': ['.tmp/public/jst-user.js']
      }
    },

    devUserJsJade: {
      options: {
        startTag: '// USER_SCRIPTS',
        endTag: '// USER_SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': require('../pipeline').jsUserFilesToInject
      }
    },

    devUserJsRelativeJade: {
      options: {
        startTag: '// USER_SCRIPTS',
        endTag: '// USER_SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': require('../pipeline').jsUserFilesToInject
      }
    },

    prodUserJsJade: {
      options: {
        startTag: '// USER_SCRIPTS',
        endTag: '// USER_SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production-user.min.js']
      }
    },

    prodUserJsRelativeJade: {
      options: {
        startTag: '// USER_SCRIPTS',
        endTag: '// USER_SCRIPTS END',
        fileTmpl: 'script(src="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production-user.min.js']
      }
    },

    devUserStylesJade: {
      options: {
        startTag: '// USER_STYLES',
        endTag: '// USER_STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public'
      },

      files: {
        'views/**/*.jade': require('../pipeline').cssUserFilesToInject
      }
    },

    devUserStylesRelativeJade: {
      options: {
        startTag: '// USER_STYLES',
        endTag: '// USER_STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public',
        relative: true
      },

      files: {
        'views/**/*.jade': require('../pipeline').cssUserFilesToInject
      }
    },

    prodUserStylesJade: {
      options: {
        startTag: '// USER_STYLES',
        endTag: '// USER_STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production-user.min.css']
      }
    },

    prodUserStylesRelativeJade: {
      options: {
        startTag: '// USER_STYLES',
        endTag: '// USER_STYLES END',
        fileTmpl: 'link(rel="stylesheet", href="%s")',
        appRoot: '.tmp/public',
        relative: true
      },
      files: {
        'views/**/*.jade': ['.tmp/public/min/production-user.min.css']
      }
    },

    // Bring in JST template object
    devUserTplJade: {
      options: {
        startTag: '// USER_TEMPLATES',
        endTag: '// USER_TEMPLATES END',
        fileTmpl: 'script(type="text/javascript", src="%s")',
        appRoot: '.tmp/public'
      },
      files: {
        'views/**/*.jade': ['.tmp/public/jst-user.js']
      }
    },

  });

  grunt.loadNpmTasks('grunt-sails-linker');
};
