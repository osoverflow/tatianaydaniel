/**
 * Created by mario on 14/11/16.
 */

module.exports = function(grunt) {

  grunt.config.set('bower', {
    dev: {
      dest: 'assets/deps',
      js_dest: 'assets/deps/js',
      css_dest: 'assets/deps/css',
      options: {
        packageSpecific: {
          gentelella: {
            files: [
              'vendors/bootstrap/dist/css/bootstrap.min.css',
              'vendors/font-awesome/css/font-awesome.min.css',
              'vendors/nprogress/nprogress.css',
              'vendors/iCheck/skins/flat/green.css',
              'vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
              'vendors/jqvmap/dist/jqvmap.min.css',
              'vendors/bootstrap-daterangepicker/daterangepicker.css',
              'build/css/custom.min.css',

              'vendors/jquery/dist/jquery.min.js',
            ]
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-bower');
};
