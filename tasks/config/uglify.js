/**
 * `uglify`
 *
 * ---------------------------------------------------------------
 *
 * Minify client-side JavaScript files using UglifyJS.
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-uglify
 *
 */
module.exports = function(grunt) {

  grunt.config.set('uglify', {
    dist: {
      src: ['.tmp/public/ngannotate/production.js'],
      dest: '.tmp/public/min/production.min.js'
    },
    distAdmin: {
      src: ['.tmp/public/ngannotate/production-admin.js'],
      dest: '.tmp/public/min/production-admin.min.js'
    },
    distUser: {
      src: ['.tmp/public/ngannotate/production-user.js'],
      dest: '.tmp/public/min/production-user.min.js'
    },
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
};
