/**
 * `concat`
 *
 * ---------------------------------------------------------------
 *
 * Concatenates the contents of multiple JavaScript and/or CSS files
 * into two new files, each located at `concat/production.js` and
 * `concat/production.css` respectively in `.tmp/public/concat`.
 *
 * This is used as an intermediate step to generate monolithic files
 * that can then be passed in to `uglify` and/or `cssmin` for minification.
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-concat
 *
 */
module.exports = function(grunt) {

  grunt.config.set('concat', {
    js: {
      src: require('../pipeline').jsFilesToInject,
      dest: '.tmp/public/concat/production.js'
    },
    css: {
      src: require('../pipeline').cssFilesToInject,
      dest: '.tmp/public/concat/production.css'
    },


    jsAdmin: {
      src: require('../pipeline').jsAdminFilesToInject,
      dest: '.tmp/public/concat/production-admin.js'
    },
    cssAdmin: {
      src: require('../pipeline').cssAdminFilesToInject,
      dest: '.tmp/public/concat/production-admin.css'
    },
    jsUser: {
      src: require('../pipeline').jsUserFilesToInject,
      dest: '.tmp/public/concat/production-user.js'
    },
    cssUser: {
      src: require('../pipeline').cssUserFilesToInject,
      dest: '.tmp/public/concat/production-user.css'
    },
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
};
