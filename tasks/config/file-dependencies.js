/**
 * Created by mario on 14/11/16.
 */

module.exports = function(grunt) {
  grunt.config.set('file_dependencies', {
    options: {
      // Task-specific options go here.
    },
    your_target: {
      // Target-specific file lists and/or options go here.
    },
  });

  grunt.loadNpmTasks('grunt-file-dependencies');

};
