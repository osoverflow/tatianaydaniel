/**
 * `prod`
 *
 * ---------------------------------------------------------------
 *
 * This Grunt tasklist will be executed instead of `default` when
 * your Sails app is lifted in a production environment (e.g. using
 * `NODE_ENV=production node app`).
 *
 * For more information see:
 *   http://sailsjs.org/documentation/anatomy/my-app/tasks/register/prod-js
 *
 */
module.exports = function(grunt) {
  grunt.registerTask('prod', [
    'browserify',
    'compileAssets',
    'concat',
    'ngAnnotate',
    'uglify',
    'cssmin',
    'sails-linker:prodJs',
    'sails-linker:prodStyles',
    'sails-linker:devTpl',
    'sails-linker:prodJsJade',
    'sails-linker:prodStylesJade',
    'sails-linker:devTplJade',


    'sails-linker:prodAdminJs',
    'sails-linker:prodAdminStyles',
    'sails-linker:devAdminTpl',
    'sails-linker:prodAdminJsJade',
    'sails-linker:prodAdminStylesJade',
    'sails-linker:devAdminTplJade',

    'sails-linker:prodUserJs',
    'sails-linker:prodUserStyles',
    'sails-linker:devUserTpl',
    'sails-linker:prodUserJsJade',
    'sails-linker:prodUserStylesJade',
    'sails-linker:devUserTplJade',
  ]);
};
