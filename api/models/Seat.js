/**
 * Seat.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    tableNumber: {
      type:'string',
      required: true
    },
    invitees: {
      collection: 'invitee',
      via: 'id'
    },
  }
};

