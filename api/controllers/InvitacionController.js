/**
 * InvitacionController
 *
 * @description :: Server-side logic for managing invitacions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  q: function (req, res) {
    User.find({select: ['username']}).where({'username': {contains: req.query.username}}).limit(5).then(function (data) {
      var ret = [];
      sails.log('Devolvio ', data);
      _.each(data, function (v) {
        ret.push(v.username);
        // ret.push({value: v.username, display: v.username});
      });
      console.log('Finaliza con ', ret);
      return res.ok(ret);
      // return res.ok([{value: 'Medina', display: 'Medina'}, {value: 'Hernandez', display: 'Hernandez'}]);
    })

  }
};

