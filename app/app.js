var angular = require('angular'),
  _ = require('lodash'),
  md5 = require('md5');
require('angular-ui-router'),
  require('angular-material'),
  require('angular-sails'),
  require('angular-local-storage');

var app = angular.module('tatsjuda', ['ui.router', 'LocalStorageModule','ngMaterial','ngSails']);

// app.directive('ngAutocomplete', );
app.controller('AuthCtrl', require('./controllers/AuthCtrl'));
app.controller('MainCtrl', require('./controllers/MainCtrl'));
app.controller('RegisterCtrl', require('./controllers/RegisterCtrl'));
app.controller('InvitacionCtrl', require('./controllers/InvitacionCtrl'));
app.controller('ReportCtrl', require('./controllers/ReportCtrl'));
app.config(require('./controllers/ConfigCtrl'));

app.run(function ($log, $state, $rootScope, $window, $http, localStorageService) {
  $rootScope.user=localStorageService.get('user')||null;


  $rootScope.setUser=function(user) {
    localStorageService.set('user',user);
    $rootScope.user=user;
  }
  $rootScope.isAuth = function() {
    if(_.has($rootScope,'user.username')) return true;
    return false;
  }
  $rootScope.unsetUser=function() {
    localStorageService.remove('user');
    localStorageService.set('user',null);
    $rootScope.user=null;
  }

  $http.get('/user/me').then(function(user) {
    console.log('Si esta activa la sesion ',user.data);
    $rootScope.setUser(user.data);
  }, function(err) {
    $log.error('No esta activa la sesion');
    $rootScope.unsetUser();
  })

});
