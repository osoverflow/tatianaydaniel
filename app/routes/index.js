import React from 'react';
import {Route, IndexRoute} from 'react-router';

import App from '../components/App';
import LoginForm from '../components/LoginForm';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={LoginForm}/>
    {/*<Route path="posts/new" component={PostsNew} />*/}
    {/*<Route path="posts/:id" component={PostsShow}/>*/}
  </Route>
);
