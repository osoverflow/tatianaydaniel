import {FETCH_INVITEES, FETCH_INVITEE} from '../actions/index';

const INITIAL_STATE = {
  all: [],
  invitee: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_INVITEES:
      return {}; //{...state, all: action.payload.data};
    case FETCH_INVITEE:
      return {...state, invitee: action.payload.data};
    default:
      return state;
  }
}
