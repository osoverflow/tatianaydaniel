/**
 * Created by mario on 10/03/17.
 */
import {AUTHENTICATE} from '../actions/index';

const INITIAL_STATE = {
  user: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AUTHENTICATE:
      /*
       console.log(action.payload.data);
       return action.payload.then((d) => {
       console.log(d);
       return {
       user: action.payload.data,
       error: ''
       }
       }).catch((err) => {
       console.log(err);
       return {
       user: null, error: 'Auth Error'
       }
       });
       */
      return {user: action.payload.data};
    default:
      return state;
  }
}
