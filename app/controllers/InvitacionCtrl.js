/**
 * Created by mario on 27/03/17.
 */
module.exports = function ($timeout, $http, $sails, $q, $log) {
  var self = this;

  self.simulateQuery = false;
  self.isDisabled = false;

  // list of `state` value/display objects
  self.states = loadAll();
  self.querySearch = querySearch;
  self.selectedItemChange = selectedItemChange;
  self.searchTextChange = searchTextChange;

  self.newState = newState;

  function newState(state) {
    alert("Sorry! You'll need to create a Constitution for " + state + " first!");
  }

  // ******************************
  // Internal methods
  // ******************************

  /**
   * Search for states... use $timeout to simulate
   * remote dataservice call.
   */
  function doSearch(query, deferred) {
    console.log('Debounced con query promise ' + query);
    $sails.get('/invitacion/q?username=' + query).then(function (res) {
      deferred.resolve(res.data);
    }, function (err) {
      deferred.resolve([]);
    });
  }

  function querySearch(query) {
    if (query.length < 3) return [];
    return $sails.get('/invitacion/q?username=' + query).then(function (res) {
      return res.data
    });
    var deferred;
    deferred = $q.defer();
    _.debounce(doSearch, 750)(query, deferred);
    return deferred.promise;
  }

  function searchTextChange(text) {
    $log.info('Text changed to ' + text);
  }

  function selectedItemChange(item) {
    $log.info('Item changed to ' + JSON.stringify(item));

  }

  /**
   * Build `states` list of key/value pairs
   */
  function loadAll() {
    var allStates = 'Medina Hernandez, Hernandez Trujillo, Trujillo Ramirez';

    return allStates.split(/, +/g).map(function (state) {
      return {
        value: state.toLowerCase(),
        display: state
      };
    });
  }

  /**
   * Create filter function for a query string
   */
  function createFilterFor(query) {
    var lowercaseQuery = angular.lowercase(query);

    return function filterFn(state) {
      return (state.value.indexOf(lowercaseQuery) === 0);
    };

  }
}
