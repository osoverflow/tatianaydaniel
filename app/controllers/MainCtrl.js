/**
 * Created by mario on 27/03/17.
 */
module.exports = function ($scope, $log, $location) {
  $scope.algo = 'PRINCIPAL';
  $scope.isActive = function (viewLocation) {
    return viewLocation === $location.path();
  };
};
