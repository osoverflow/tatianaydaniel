/**
 * Created by mario on 18/04/17.
 */
module.exports = function ($scope, $timeout, $http, $sails, $q, $log) {
  $scope.report=[];
  $http.get('/invitee?limit=0&sort=confirmed+desc').then(function(report) {
    $scope.report=report.data;
    $log.debug(report);
  }, function (err) { $log.error(err); })
}
