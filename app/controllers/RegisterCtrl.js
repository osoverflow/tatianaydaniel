/**
 * Created by mario on 27/03/17.
 */
/**
 * Created by mario on 27/03/17.
 */
module.exports = function ($rootScope, $scope, $log, $state, $http) {
  if (!$rootScope.isAuth()) {
    $state.go("login");
  } else {
    $scope.invitees = [];

    $scope.getInvitees = function () {
      $http.get('/invitee').then(function (invitees) {
        $scope.invitees = invitees.data;
      }, function (err) {
      })
    }

    $scope.getInvitees();

    $scope.toggle=function(invitee) {
      console.log('Toggle invitee ',invitee);
      $http.put('/invitee/'+invitee.id,{confirmed:!invitee.confirmed}).then(function(_invitee) {
        invitee.confirmed=_invitee.data.confirmed;
      },function(err) {})

    }
  }
};
