/**
 * Created by mario on 27/03/17.
 */
var md5 = require('md5');
module.exports = function ($rootScope, $scope, $log, $state, $http, $sails) {
  var self=this;
  self.auth = {
    identifier: '',
    password: '',
    rememberMe: null
  };
  if($state.current.name=='logout') {
    $rootScope.unsetUser();
    $state.go("login");
  }
  $scope.doLogin = function() {
    var password=self.auth.password;
    if(password.length<3) password=md5(password);
    $sails.post("/auth/local",{identifier:self.auth.identifier, password: password}).then(function(user) {
      $log.info('Accedio',user.data);
      $rootScope.setUser(user.data);
      $state.go('registro');
    }, function(err) {
      $log.error('Error',err);
    })
    console.log('Entrando con ',self.auth,md5(self.auth.password))
  }

};
