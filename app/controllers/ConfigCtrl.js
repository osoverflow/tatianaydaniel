/**
 * Created by mario on 27/03/17.
 */
module.exports = function ($stateProvider, $urlRouterProvider, localStorageServiceProvider) {
  $stateProvider.state('home', {
    url: '/',
    template: window.JST['assets/app/views/home.tmpl.html'](),
    // controller: 'MainCtrl'
  });

  $stateProvider.state('registro', {
    url: '/registro',
    template: window.JST['assets/app/views/registro.tmpl.html'](),
    // controller: 'MainCtrl'
  });

  $stateProvider.state('login', {
    url: '/login',
    template: window.JST['assets/app/views/login-form.tmpl.html'](),
    // controller: 'AuthCtrl'
  });

  $stateProvider.state('logout', {
    url: '/logout',
    template: window.JST['assets/app/views/login-form.tmpl.html'](),
    // controller: 'AuthCtrl'
  });

  $stateProvider.state('mapa', {
    url: '/mapa',
    template: window.JST['assets/app/views/mapas.tmpl.html'](),
    // controller: 'AuthCtrl'
  });

  $stateProvider.state('mesa', {
    url: '/mesa',
    template: window.JST['assets/app/views/mesa-regalos.tmpl.html'](),
    // controller: 'AuthCtrl'
  });

  $stateProvider.state('reporte', {
    url: '/reporte',
    template: window.JST['assets/app/views/reporte.tmpl.html'](),
    // controller: 'AuthCtrl'
  });

  $urlRouterProvider.otherwise('/registro');


  localStorageServiceProvider
    .setNotify(true, true);

};
