/**
 * Created by mario on 7/03/17.
 */
import axios from 'axios';

export const FETCH_INVITEES = 'FETCH_INVITEES';
export const FETCH_INVITEE = 'FETCH_INVITEE';
export const AUTHENTICATE = 'AUTHENTICATE';
const ROOT_URL = 'https://tatianaydaniel.herokuapp.com';

export function fetchInvitees() {
  const url = `${ROOT_URL}/invitee`;
  const request = axios.get(url);
  return {
    type: FETCH_INVITEES,
    payload: request
  };
}

export function authenticate(auth) {
  const url = `${ROOT_URL}/auth/local`;
  const request = axios.post(url, auth);
  return {
    type: AUTHENTICATE,
    payload: request
  };
}
